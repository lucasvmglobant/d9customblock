<?php

namespace Drupal\kadabrait_content;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Component\Serialization;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\node\Entity\Node;

/**
 * Prepares the salutation to the world.
 */
class GetList {

  use StringTranslationTrait;

  /**
   * Returns the nodes by articles render array.
   */
  public function getList($limit) {

    // Set properties to filter.
    $entity = \Drupal::entityTypeManager()->getStorage('node');
        
    $query = \Drupal::entityQuery('node')
      ->condition('status', 1) //published or not
      ->condition('type', 'article') //content type
      ->pager($limit); //specify results to return
    $nids = $query->execute();
  
    // Load multiples or single item load($id)
    $list = [];
    $node_storage = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($nids);
    
    foreach($node_storage as $record) {
      
      $list[] = $record->getTitle();

  }  

  return $list;
        
  }


}