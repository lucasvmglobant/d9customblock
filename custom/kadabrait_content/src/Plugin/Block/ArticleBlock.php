<?php

namespace Drupal\kadabrait_content\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Database\Connection;

/**
* Provides a block.
*
* @Block(
* id = "kadabrait_content_article_block",
* admin_label = @Translation("Kadabra IT Custom Block")
* )
*/

class ArticleBlock extends BlockBase implements ContainerFactoryPluginInterface {
 protected $database;
 protected $currentUser;

 public function __construct(array $configuration, $plugin_id, $plugin_definition,
   AccountInterface $current_user,
   Connection $database) {

   parent::__construct($configuration, $plugin_id, $plugin_definition);
   $this->currentUser = $current_user;
   $this->database = $database;
 }

 public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
   return new static(

     $configuration,
     $plugin_id,
     $plugin_definition,
     $container->get('current_user'),
     $container->get('database'),
     $container->get('kadabrait_content_list')

   );
 }

 public function build() {

  $limit = $this->configuration['node_number'];
  $block_message = $this->configuration['block_message']; 
  //Call service 
  $callService = \Drupal::service('kadabrait_content_list');
  $getList = $callService->getList($limit);

  if (empty($getList)) {
    $build[] = [
      '#markup' => '<h3>' . $this->t('No results found') . '</h3>',
   ];
  } else {

  $build[] = [
    '#theme' => 'item_list',
    '#items' => $getList,
    '#cache' => ['max-age' => 0],
  ];
  }

  return $build;
  }
 /**
  * {@inheritdoc}
 */ 
 public function blockForm($form, FormStateInterface $form_state) {

  $form['kadabrait_blocks_block_message'] = array(
    '#type' => 'textfield',
    '#title' => $this->t('Display message'),
    '#default_value' => $this->configuration['block_message'],
  );
 
  $range = range(1, 10);

  $form['kadabrait_blocks_node_number'] = array(
    '#type' => 'select',
    '#title' => $this->t('Number of nodes'),
    '#default_value' => $this->configuration['node_number'],
    '#options' => array_combine($range, $range),
  );

  return $form;

  }

  public function blockValidate($form, FormStateInterface $form_state) {

    if (strlen($form_state->getValue('kadabrait_blocks_block_message')) < 10) {

      $form_state->setErrorByName('kadabrait_blocks_block_message',
      $this->t('The text must be at least 10 characters long'));

    }
  }

  public function blockSubmit($form, FormStateInterface $form_state) {

    $this->configuration['block_message'] = $form_state->getValue('kadabrait_blocks_block_message');
    $this->configuration['node_number'] = $form_state->getValue('kadabrait_blocks_node_number');

  }

  public function defaultConfiguration() {

    return array(
      'block_message' => 'Show list of nodes from Article Content type',
      'node_number' => 5,
    );

  }

}
